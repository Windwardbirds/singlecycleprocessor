module regfile (input logic clk, we3,
					input logic [4:0] ra1, ra2, wa3,
					input logic [63:0] wd3,
					output logic [63:0] rd1, rd2);
					
	logic [63:0] regmem [0:31] = '{63'h00000000, 63'h00000001, 63'h000000002, 63'h00000003,
											63'h00000004, 63'h00000005, 63'h00000006, 63'h00000007,
											63'h00000008, 63'h00000009, 63'h0000000a, 63'h0000000b,
											63'h0000000c, 63'h0000000d, 63'h0000000e, 63'h0000000f,
											63'h00000010, 63'h00000011, 63'h00000012, 63'h00000013,
											63'h00000014, 63'h00000015, 63'h00000016, 63'h00000017,
											63'h00000018, 63'h00000019, 63'h0000001a, 63'h0000001b,
											63'h0000001c, 63'h0000001d, 63'h0000001e, 63'h00000000};

	always_ff @(posedge clk)
		if (we3) if (wa3 !== 5'b11111) regmem[wa3] <= wd3;
		
	assign rd1 = regmem[ra1];
	assign rd2 = regmem[ra2];
	
endmodule
