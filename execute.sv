module execute #(parameter N = 64)
					(input logic AluSrc,
					input logic [3:0] AluControl,
					input logic [63:0] PC_E, signImm_E, readData1_E, readData2_E,
					output logic [63:0] PCBranch_E, aluResult_E, writeData_E,
					output logic zero_E);
					

	logic [63:0] shifted_imm, selected_rm;

	sl2 #(64) shift_imm(signImm_E, shifted_imm);
	adder #(64) compute_branch_addr(PC_E, shifted_imm, PCBranch_E);
	
	mux2 #(64) data_or_signimm(readData2_E, signImm_E, AluSrc, selected_rm);
	alu actual_alu(readData1_E, selected_rm, AluControl, aluResult_E, zero_E);
	

	assign writeData_E = readData2_E;
	
endmodule
