transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/sl2.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/signext.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/regfile.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/mux2.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/maindec.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/imem.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/flopr.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/fetch.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/execute.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/alu.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/adder.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/writeback.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/processor_arm.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/memory.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/decode.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/datapath.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/controller.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/aludec.sv}
vcom -2008 -work work {C:/Users/Franco/Documents/quartus/practicodos/dmem.vhd}

vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/practicodos {C:/Users/Franco/Documents/quartus/practicodos/processor_tb.sv}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cycloneive_ver -L rtl_work -L work -voptargs="+acc"  processor_tb

add wave *
view structure
view signals
run -all
